import mysql.connector
from mysql.connector import Error

def connect(func):
    try:
        conn = mysql.connector.connect(host='localhost',
                                       database='schedule',
                                       user='mysql_user',
                                       password='mysql123')
        if conn.is_connected():
            func(conn.cursor())
            conn.commit()

    except Error as e:
        print(e)

    finally:
        conn.close()

def removePlus(cursor):
    cursor.execute("""SELECT id, (SELECT location FROM event_templates WHERE week_id=a.week_id AND day_id=a.day_id AND event_index=a.event_index AND group_id=a.group_id and location<>'+') new_loc FROM `event_templates` a WHERE location='+'""")
    rows = cursor.fetchall()
    for row in rows:
        loc=""
        if row[1]!=None:
            loc=row[1]
        cursor.execute("UPDATE event_templates SET location=%s WHERE id=%s",[loc, row[0]])
        print(str(row[0])+" "+loc)

def removeNN(cursor):
    cursor.execute("""SELECT id, (SELECT teacher_id FROM event_templates WHERE week_id=a.week_id AND day_id=a.day_id AND event_index=a.event_index AND group_id=a.group_id and teacher_id<>1) new_loc FROM `event_templates` a WHERE teacher_id=1""")
    rows = cursor.fetchall()
    for row in rows:
        if row[1]==None:
            continue
        cursor.execute("UPDATE event_templates SET teacher_id=%s WHERE id=%s",[row[1], row[0]])
        print(str(row[0])+" "+str(row[1]))

if __name__ == '__main__':
    connect(removePlus)
    connect(removeNN)