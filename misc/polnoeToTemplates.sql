INSERT INTO `event_templates`(`week_id`, `day_id`, `event_index`, `course_id`, `type_id`, `split_groups_number`, `teacher_id`, `group_id`, `location`) 
SELECT 
(CASE ord(`WEEK`) 
	when 53405 then 1
    when 53415 then 2
    when 0 then 3
    else 1
end)
    week_id, 
`DAY` day_id, `LES` event_index, (SELECT `id` FROM `courses` WHERE i.`SUBJECT` = `name` LIMIT 1) course_id, (SELECT `id` FROM `event_types` WHERE `name` = i.`SUBJ_TYPE`) type, IF(`SUBG`='',0,`SUBG`) split_groups_number, (SELECT `id` FROM `teachers` WHERE `name` = i.`NAME` LIMIT 1) teacher_id, (SELECT `id` FROM `groups` WHERE `name` = i.`GROUP` LIMIT 1) group_id, `AUD` location FROM `polnoe` as i