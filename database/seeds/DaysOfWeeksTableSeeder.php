<?php

use Illuminate\Database\Seeder;

class DaysOfWeeksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('days_of_weeks')->insert(['name' => 'Понедельник']);
        DB::table('days_of_weeks')->insert(['name' => 'Вторник']);
        DB::table('days_of_weeks')->insert(['name' => 'Среда']);
        DB::table('days_of_weeks')->insert(['name' => 'Четверг']);
        DB::table('days_of_weeks')->insert(['name' => 'Пятница']);
        DB::table('days_of_weeks')->insert(['name' => 'Суббота']);
        DB::table('days_of_weeks')->insert(['name' => 'Воскресенье']);
    }
}
