<?php

use Illuminate\Database\Seeder;

class WeekTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('week_types')->insert(['name' => 'Нечетная']);
        DB::table('week_types')->insert(['name' => 'Четная']);
        DB::table('week_types')->insert(['name' => 'На обеих']);
    }
}
