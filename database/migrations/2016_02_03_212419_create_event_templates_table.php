<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('week_id')->unsigned();
            $table->integer('day_id')->unsigned();
            $table->integer('event_index');
            $table->integer('course_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('split_groups_number')->default(0);
            $table->integer('teacher_id')->unsigned();
            $table->integer('group_id')->unsigned();
            $table->string('location', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_templates');
    }
}
