<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFieldInEventTemlates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_templates', function($table)
        {
            $table->renameColumn('split_groups_number', 'subgroup');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_templates', function($table)
        {
            $table->renameColumn('subgroup', 'split_groups_number');
        });
    }
}
