$(function () {

    var Main = {

        run: function () {
            this.footerMove();
            this.select2init();
            this.scheduleBuilder();
            this.hideSearch();
            if (!localStorage.getItem('group_id_16022016')  && !localStorage.getItem('teacher_id_16022016')) {
                this.viewSearch();
            }
        },
        resizeFunctions: function () {
            if ($(window).outerWidth() > 560) {
                $('.head-search-form').show();
            } else {
                //$('.head-search-form').hide();
            }
        },
        scrollFunctions: function () {
            this.headMinification();
        },
        afterTableLoad: function () {
            this.viewTable();
            this.date();
            this.select2init();
            this.hideGroupsComponent();
            this.changeWeek();
            //this.openHomework();
            this.editArea();
            this.setToCurrent();
           // this.editMode();
        },

        hideSearch: function () {
            $('.group-name').on('click', function () {
                if ($(window).outerWidth() <= 560) {
                    $('.head-search-form').toggle()
                } else {
                    $('.head-search-form').show()
                }
            });
            $(document).on('click', function (event) {
                if ($(window).outerWidth() <= 560) {
                    if ($(event.target).closest(".group-name, .head-search-form").length)
                        return;
                    $(".head-search-form").hide();
                    console.log(2);
                    event.stopPropagation();
                } else {
                    $('.head-search-form').show()
                }
            });
        },

        editMode: function () {
            var md5_check = '3ab48f05066798b72f909e0dfcda9bc6';
            $(document).keyup(function (eventObject) {
                if (eventObject.which == 13 && md5($.trim($('.head-search-form .select2-search__field').val())) == md5_check) {
                    alert('Режим редактирования активирован!');
                    $('.head-search-form .select2-search__field').val(null);
                }
            });
        },


        setToCurrent: function () {
            scrollToCurrentDay();
            accentCurrentLess();
            setInterval(accentCurrentLess(), 10000);
            function scrollToCurrentDay() {
                var date = new Date();
                var week = (date.getWeek() + 1) % 2;
                var day = date.getDay();
                var canScroll = true;
                $(document).on('click scroll mousedown touchstart', function () {
                    canScroll = false
                });
                if (week == 1) {
                    $('#week-2').removeClass('hide');
                    $('#week-1').addClass('hide');
                } else {
                    $('#week-1').removeClass('hide');
                    $('#week-2').addClass('hide');
                }
                setTimeout(function () {
                    var day_selector = '#day-' + (week + 1) + '-' + day;
                    if (canScroll && $(day_selector).offset().top + +$(day_selector).outerHeight() > window.pageYOffset + window.innerHeight) {
                        $.scrollTo(day_selector, 1600, {queue: true, offset: -47});
                    }
                }, 2500);
            }

            function accentCurrentLess() {
                var date = new Date();
                var week = (date.getWeek() + 1) % 2;
                var day = date.getDay();
                var time = date.getHours() * 60 + date.getMinutes();
                var lessNum = 0;
                switch (true) {
                    case (8 * 60 + 00 < time && time < 9 * 60 + 30):
                        lessNum = 1;
                        break;
                    case (9 * 60 + 40 < time && time < 11 * 60 + 20):
                        lessNum = 2;
                        break;
                    case (11 * 60 + 20 < time && time < 12 * 60 + 50):
                        lessNum = 3;
                        break;
                    case (13 * 60 + 30 < time && time < 15 * 60 + 00):
                        lessNum = 4;
                        break;
                    case (15 * 60 + 10 < time && time < 16 * 60 + 40):
                        lessNum = 5;
                        break;
                    case (16 * 60 + 50 < time && time < 18 * 60 + 20):
                        lessNum = 6;
                        break;
                    case (18 * 60 + 30 < time && time < 20 * 60 + 00):
                        lessNum = 7;
                        break;
                    case (20 * 60 + 10 < time && time < 21 * 60 + 40):
                        lessNum = 8;
                        break;
                    default:
                        break;
                }
                var less_selector = '#less-' + (week + 1) + '-' + day + '-' + lessNum;
                $(less_selector).parent().addClass('currentLess');
            }

        },
        headMinification: function () {
            if (window.pageYOffset > $('.head').outerHeight()) {
                $('.head-min').slideDown('min');
            } else {
                $('.head-min').slideUp('min');
            }
        },
        viewTable: function () {
            $('.search-section').fadeOut();
            setTimeout(function () {
                $('main').fadeIn();
            }, 500);
        },
        viewSearch: function () {
            $('.search-section').fadeIn();
        },
        editArea: function () {
            function starEditing(editBtnElement) {
                var _this = $(editBtnElement);
                var editAreaWrap = _this.parent();
                var editArea = editAreaWrap.find('.editArea');
                var text = $.trim(editArea.text());
                editArea.after('<textarea class="edit-area">' + text + '</textarea>');
                editArea.detach();
                _this.addClass('hide');
                _this.parent().find('.refresh-btn').addClass('hide');
                _this.parent().find('.submit-edit-btn').removeClass('hide');
            }

            function endEditing(sbmtEditBtnElement) {
                var _this = $(sbmtEditBtnElement);
                var editAreaWrap = _this.parent();
                var editText = editAreaWrap.find('textarea.edit-area');
                if (editText) {
                    var text = $.trim(editText.val());
                    editText.after('<div class="editArea"></div>');
                    editText.detach();
                    editAreaWrap.find('.editArea').text(text);
                    _this.addClass('hide');
                    _this.parent().find('.edit-btn').removeClass('hide');
                    _this.parent().find('.refresh-btn').removeClass('hide');
                }
            }

            $('.edit-btn').on('click', function () {
                $('.submit-edit-btn:not(".hide")').each(function () {
                    endEditing(this)
                });
                starEditing(this)
            });
            $('.submit-edit-btn').on('click', function () {
                endEditing(this)
            });
        },
        select2init: function () {
            $('#group-select, #head-group-select').select2({
                multiple: true,
                maximumSelectionLength: 1,
                minimumInputLength: 1,
                placeholder: "Найди группу...",
                language: {
                    inputTooShort: function () {
                        return 'Введите хотя бы 1 букву';
                    },
                    maximumSelected: function () {
                        return 'Ошибка. Попробуёте ещё раз';
                    },
                    searching: function () {
                        return 'Поиск…';
                    },
                    noResults: function () {
                        return 'Совпадений не найдено';
                    },
                    loadingMore: function () {
                        return 'Загрузка данных…';
                    },
                    errorLoading: function () {
                        return 'Невозможно загрузить результаты';
                    }
                },
                ajax: {
                    url: '/api/v1/groups/',
                    dataType: "json",
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    results: function (data) {
                        return {results: data, text: 'name'};
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    formatSelection: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });
            $('#teacher-select,#head-teacher-select').select2({
                multiple: true,
                maximumSelectionLength: 1,
                minimumInputLength: 1,
                placeholder: "Найди преподавателя...",
                language: {
                    inputTooShort: function () {
                        return 'Введите хотя бы 1 букву';
                    },
                    maximumSelected: function () {
                        return 'Ошибка. Попробуёте ещё раз';
                    },
                    searching: function () {
                        return 'Поиск…';
                    },
                    noResults: function () {
                        return 'Совпадений не найдено';
                    },
                    loadingMore: function () {
                        return 'Загрузка данных…';
                    },
                    errorLoading: function () {
                        return 'Невозможно загрузить результаты';
                    }
                },
                ajax: {
                    url: '/api/v1/teachers/',
                    dataType: "json",
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    results: function (data) {
                        return {results: data, text: 'name'};
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    formatSelection: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });
        },
        changeWeek: function () {
            $('.week-name').on('click', function () {
                $('.week').toggleClass('hide');
            });
        },
        openHomework: function () {
            $(".less").on('click', function () {
                $(".info").not($(this).next(".info")).slideUp();
                $(this).next(".info").slideToggle();
            });
        },
        hideGroupsComponent: function () {
            var btn1 = $('.subgroup-btn-1');
            var btn2 = $('.subgroup-btn-2');
            var btn3 = $('.subgroup-btn-3');
            var group1 = $('.group-1');
            var group2 = $('.group-2');
            var group3 = $('.group-3');
            var div = $('div');
            btn1.show();
            btn2.show();
            btn3.show();
            if (!div.is('.group-1')) {
                btn1.hide();
            }
            if (!div.is('.group-2')) {
                btn2.hide();
            }
            if (!div.is('.group-3')) {
                btn3.hide();
            }
            function showG(group) {
                group.parent().slideDown().removeClass('folded');
                group.parent().parent().addClass('haveLess');
                group.parent().parent().parent().removeClass('empty');
            }

            function hideG(group) {
                group.parent().slideUp().addClass('folded');
                group.each(function () {
                    var _this = $(this);
                    var f = _this.parent().parent().parent().find('.less-wrap').is(':not(".folded")');
                    if (!f) {
                        _this.parent().parent().removeClass('haveLess');
                        _this.parent().parent().parent().addClass('empty');
                    }
                });
            }

            btn1.on('click', function () {
                $(":not('.group-1') + .info").slideUp();
                showG(group1);
                hideG(group2);
                hideG(group3);
            });
            btn2.on('click', function () {
                $(":not('.group-2') + .info").slideUp();
                showG(group2);
                hideG(group1);
                hideG(group3);
            });
            btn3.on('click', function () {
                $(":not('.group-3') + .info").slideUp();
                showG(group3);
                hideG(group1);
                hideG(group2);
            });
            $('.subgroup-btn-0').on('click', function () {
                showG(group1);
                showG(group2);
                showG(group3);
            });
        },
        date: function () {
            var days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
            var months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря',];

            function date() {
                var date = new Date();
                $('.date').text(date.getDate() + ' ' + months[date.getMonth()]);
                $('.hours').text(date.getHours());
                $('.minutes').text(date.getMinutes());
                $('.weekday').text(days[date.getDay() - 1]);
            }

            date();
            setInterval(date, 30000);
        },

        scheduleBuilder: function () {

            if (localStorage.getItem('group_id_16022016')) {
                getSchedule(localStorage.getItem('group_id_16022016'));
            }

            if (localStorage.getItem('teacher_id_16022016')) {
                getTeacherSchedule(localStorage.getItem('teacher_id_16022016'));
            }

            $('#teacher-select, #head-teacher-select').on('change', function () {
                var select = $(this);
                if (select.val()) {
                    var teacher_id = select.val();
                    var teacher_name = select.select2('data')[0].text;
                    localStorage.setItem('teacher_id_16022016', teacher_id);
                    localStorage.setItem('teacher_name', teacher_name);
                    getTeacherSchedule(teacher_id);
                }
            });

            $('#group-select, #head-group-select').on('change', function () {
                var select = $(this);
                if (select.val()) {
                    var group_id = select.val();
                    var group_name = select.select2('data')[0].text;
                    localStorage.setItem('group_id_16022016', group_id);
                    localStorage.setItem('group_name', group_name);
                    getSchedule(group_id);
                }
            });
            

            function getSchedule(group_id) {
                $.ajax({
                    url: "/api/v1/groups/" + group_id + "/schedule",
                    dataType: "json",
                    success: function (data) {
                        //console.log(data);
                        mainTableBuilder();
                        allocateLessons(data);
                        $(".group-name").text(localStorage.getItem('group_name'));
                        document.title = localStorage.getItem('group_name') + ' - Расписание МГТУ им. Носова';
                        $('select').val(null).trigger("change");
                        $(document).trigger('getScheduleComplete');
                    }
                });
            }


            function getTeacherSchedule(teacher_id) {
                $.ajax({
                    url: "/api/v1/teachers/" + teacher_id + "/schedule",
                    dataType: "json",
                    success: function (data) {
                        //console.log(data);
                        mainTableBuilder();
                        allocateTeacherLessons(data);
                        $(".group-name").text(localStorage.getItem('teacher_name'));
                        document.title = localStorage.getItem('teacher_name') + ' - Расписание МГТУ им. Носова';
                        $('select').val(null).trigger("change");
                        $(document).trigger('getScheduleComplete');
                    }
                });
            }

            function allocateLessons(data) {
                for (var w = 0; w < data.length; w++) {
                    var week = data[w];
                    for (var d = 0; d < week['days'].length; d++) {
                        var day = week['days'][d];
                        for (var l = 0; l < day['events'].length; l++) {
                            var event = day['events'][l];
                            var lessonHtml = '' +
                                '<div class="less-wrap">' +
                                '<div class="less group-' + event['subgroup'] + '">' +
                                '<div class="title">' + event['course'] + '</div>' +
                                '<div class="ad clearfix">' + event['type'] + '' +
                                '<div class="teacher">' + event['teacher'] + '</div>' +
                                '</div>' +
                                '<div class="aud">' + event['location'] + '</div>' +
                                '</div>';
                            //'<div class="info">' +
                            //'<div class="edit-btn pencil-edit btn"></div>' +
                            //'<div class="homework">http://www.dezinfo.net/images4/image/10.2013/education/education_03.jpg конспект по параграфу 5. П</div>' +
                            //'<button class="submit-edit-btn hide">Готово</button>' +
                            //'</div>' +
                            //'</div>';
                            var lessonCell = $('#less-' + week['week_id'] + '-' + day['day_id'] + '-' + event['event_index']);
                            var dayCell = $('#day-' + week['week_id'] + '-' + day['day_id']);
                            lessonCell.addClass('haveLess');
                            dayCell.addClass('haveLess');
                            lessonCell.parent().removeClass('empty');
                            lessonCell.append(lessonHtml);
                        }
                    }
                }
                $.each($('.day'), function(){
                    var _this = $(this);
                    _this.find('table tr:not(.empty)').last().nextAll('.empty').detach();
                })
            }
            function allocateTeacherLessons(data) {
                for (var w = 0; w < data.length; w++) {
                    var week = data[w];
                    for (var d = 0; d < week['days'].length; d++) {
                        var day = week['days'][d];
                        for (var l = 0; l < day['events'].length; l++) {
                            var event = day['events'][l];
                            var lessonHtml = '' +
                                '<div class="less-wrap">' +
                                '<div class="less group-' + event['subgroup'] + '">' +
                                '<div class="title">' + event['course'] + '</div>' +
                                '<div class="ad clearfix">' + event['type'] + '' +
                                '<div class="teacher">' + event['teacher'] + '</div>' +
                                '</div>' +
                                '<div class="aud">' + event['location'] + '</div>' +
                                '</div>';
                            //'<div class="info">' +
                            //'<div class="edit-btn pencil-edit btn"></div>' +
                            //'<div class="homework">http://www.dezinfo.net/images4/image/10.2013/education/education_03.jpg конспект по параграфу 5. П</div>' +
                            //'<button class="submit-edit-btn hide">Готово</button>' +
                            //'</div>' +
                            //'</div>';
                            var lessonCell = $('#less-' + week['week_id'] + '-' + day['day_id'] + '-' + event['event_index']);
                            var dayCell = $('#day-' + week['week_id'] + '-' + day['day_id']);
                            lessonCell.addClass('haveLess');
                            dayCell.addClass('haveLess');
                            lessonCell.parent().removeClass('empty');
                            lessonCell.append(lessonHtml);
                        }
                    }
                }
                $.each($('.day'), function(){
                    var _this = $(this);
                    _this.find('table tr:not(.empty)').last().nextAll('.empty').detach();
                })
            }

            function mainTableBuilder() {

                $('.main-table-wrap').html(' ');
                var tableHtml = '';
                var days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
                for (var w = 0; w < 2; w++) {
                    if (w == 0) {
                        tableHtml += '<div class="week" id="week-1">' +
                            '<div class="week-name">Нечётная</div>';
                    } else {
                        tableHtml += '<div class="week hide" id="week-2">' +
                            '<div class="week-name">Чётная</div>';
                    }
                    tableHtml += '<div class="main-table">' +
                        '<table>' +
                        '<tr>';
                    for (var d = 0; d < 6; d++) {
                        tableHtml += '<td class="col-xs-12 col-sm-6 col-md-4">' + '<div class="day" id="day-' + (w + 1) + '-' + (d + 1) + '">' +
                            '<div class="day-name">' + days[d] + '</div>' +
                            '<table>';
                        for (var l = 0; l < 8; l++) {
                            tableHtml += '<tr class="empty">' +
                                '<td class="less-' + (l + 1) + '" id="less-' + (w + 1) + '-' + (d + 1) + '-' + (l + 1) + '"></td>' +
                                '</tr>';
                        }
                        tableHtml += '</table>' +
                            '</div>' +
                            '</td>';
                        if (d == 1 || d == 3) {
                            tableHtml += '<td class="col-sm-12 visible-sm separator"></td>';
                        }
                        if (d == 2) {
                            tableHtml += '<td class="col-md-12 visible-md visible-lg separator"></td>';
                        }
                    }
                    tableHtml += '</tr>' +
                        '</table>' +
                        '</div>' +
                        '</div>';

                }
                $('.main-table-wrap').html(tableHtml);
            }
        },

        footerMove: function () {
            var ua = navigator.userAgent.toLowerCase();
            var isOpera = (ua.indexOf('opera') > -1);
            var isIE = (!isOpera && ua.indexOf('msie') > -1);
            var viewportHeight = getViewportHeight();
            var wrapper = $("main");
            var footer = $("footer");

            function getViewportHeight() {
                return ((document.compatMode || isIE) && !isOpera)
                    ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight;
            }

            wrapper.css("min-height", viewportHeight - footer.outerHeight(true));
        }
    };
    window.Application.addComponent("Main", Main);
});
