$(function () {

    var Main = {

        run: function () {
            //this.footerMove();
            if (localStorage.getItem('group_id')>0) {
                
            }
            this.select2init();

            $('input[type="tel"]').inputmask("+7 (999) 999-99-99", {
                showMaskOnHover: false,
                placeholder: "+7 (   )          "
            });
        },
        resizeFunctions: function () {

        },
        scrollFunctions: function () {

        },
        afterTableLoad: function () {
            this.viewTable();

            this.date();
            this.select2init();
            this.hideGroupsComponent();
            this.openHomework();
            this.editHomework();
            this.changeWeek();
        },

        viewTable: function () {
            $('.search-section').fadeOut();
            setTimeout(function () {
                $('main').fadeIn();
            }, 500);
        },
        fixedSubgBtns: function () {
            var subgroupbtns = $('.subgroup-btns');
            var delta = $(window).scrollTop() - subgroupbtns.offset().top;
            console.log(delta);
            if (delta >= 0) {
                subgroupbtns.addClass('fixedSubg');
            } else if (delta < 0) {
                subgroupbtns.removeClass('fixedSubg');
            }
        },
        editHomework: function () {
            function starEditing(editBtnElement) {
                var _this = $(editBtnElement);
                var info = _this.parent();
                var homework = info.find('.homework');
                var text = $.trim(homework.text());
                homework.after('<textarea class="edit-area">' + text + '</textarea>');
                homework.detach();
                _this.addClass('hide');
                _this.parent().find('.submit-edit-btn').removeClass('hide');
            }

            function endEditing(sbmtEditBtnElement) {
                var _this = $(sbmtEditBtnElement);
                var info = _this.parent();
                var homeworkEditText = info.find('.edit-area');
                if (homeworkEditText) {
                    var text = $.trim(homeworkEditText.val());
                    homeworkEditText.after('<div class="homework"></div>');
                    homeworkEditText.detach();
                    info.find('.homework').text(text);
                    _this.addClass('hide');
                    _this.parent().find('.edit-btn').removeClass('hide');
                }
            }

            $('.edit-btn').on('click', function () {
                $('.submit-edit-btn:not(".hide")').each(function(){
                    endEditing(this)
                });
                starEditing(this)
            });
            $('.submit-edit-btn').on('click', function () {
                endEditing(this)
            });
        },
        select2init: function () {
            $('.head-select,.head-group-select').select2({
                multiple: true,
                maximumSelectionLength: 1,
                minimumInputLength: 1,
                placeholder: "Найди свою группу...",
                language: {
                    inputTooShort: function () {
                        return 'Введите хотя бы 1 букву';
                    },
                    maximumSelected: function (args) {
                        return 'Нажмите на кнопку поиска';
                    },
                    searching: function () {
                        return 'Поиск…';
                    },
                    noResults: function () {
                        return 'Совпадений не найдено';
                    },
                    loadingMore: function () {
                        return 'Загрузка данных…';
                    },
                    errorLoading: function () {
                        return 'Невозможно загрузить результаты';
                    }
                },
                ajax: {
                    url: '/api/v1/groups/',
                    dataType: "json",
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    results: function (data) {
                        return {results: data, text: 'name'};
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    },
                    formatSelection: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            });
        },
        changeWeek: function () {
            $('.week-name').on('click', function () {
                $('.week').toggleClass('hide');
            });
        },
        openHomework: function () {
            /*$(".less").on('click', function () {
                $(".info").not($(this).next(".info")).slideUp();
                $(this).next(".info").slideToggle();
            });*/
        },
        hideGroupsComponent: function () {
            var btn1 = $('.subgroup-btn-1');
            var btn2 = $('.subgroup-btn-2');
            var btn3 = $('.subgroup-btn-3');
            var group1 = $('.group-1');
            var group2 = $('.group-2');
            var group3 = $('.group-3');
            var div = $('div');
            if (!div.is('.group-1')) {
                btn1.hide();
            }
            if (!div.is('.group-2')) {
                btn2.hide();
            }
            if (!div.is('.group-3')) {
                btn3.hide();
            }
            function showG(group) {
                group.parent().slideDown().removeClass('folded')
                group.parent().parent().addClass('haveLess');
                group.parent().parent().parent().removeClass('empty');
            }

            function hideG(group) {
                group.parent().slideUp().addClass('folded');
                group.each(function () {
                    var _this = $(this);
                    var f = _this.parent().parent().parent().find('.less-wrap').is(':not(".folded")');
                    if (!f) {
                        _this.parent().parent().removeClass('haveLess');
                        _this.parent().parent().parent().addClass('empty');
                    }
                });
            }

            btn1.on('click', function () {
                $(":not('.group-1') + .info").slideUp();
                showG(group1);
                hideG(group2);
                hideG(group3);
            });
            btn2.on('click', function () {
                $(":not('.group-2') + .info").slideUp();
                showG(group2);
                hideG(group1);
                hideG(group3);
            });
            btn3.on('click', function () {
                $(":not('.group-3') + .info").slideUp();
                showG(group3);
                hideG(group1);
                hideG(group2);
            });
            $('.subgroup-btn-0').on('click', function () {
                showG(group1);
                showG(group2);
                showG(group3);
            });
        },
        date: function () {
            function date() {
                var date = new Date();
                $('.date').text(date.toLocaleString("ru", {
                    year: 'numeric', month: 'long', day: 'numeric'
                }));
                $('.hours').text(date.toLocaleString("ru", {hour: 'numeric'}));
                $('.minutes').text(date.toLocaleString("ru", {minute: 'numeric'}));
                $('.weekday').text(date.toLocaleString("ru", {weekday: 'long'}));
            }

            date();
            setInterval(date, 30000);
        },
        footerMove: function () {
            var ua = navigator.userAgent.toLowerCase();
            var isOpera = (ua.indexOf('opera') > -1);
            var isIE = (!isOpera && ua.indexOf('msie') > -1);
            var viewportHeight = getViewportHeight();
            var wrapper = $("main");
            var footer = $("footer");

            function getViewportHeight() {
                return ((document.compatMode || isIE) && !isOpera)
                    ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight;
            }

            wrapper.css("min-height", viewportHeight - footer.outerHeight(true));
        }

    };
    window.Application.addComponent("Main", Main);


});