window.Application = {
    Components: {},
    /**
     * Front controller application, init all plugin
     * and event handler
     */

    addComponent: function (name, object) {
        this.Components[name] = object;
        object.run();
        if (object.resizeFunctions != null && typeof(object.resizeFunctions) == "function") {
            $(window).on("resize", function () {
                object.resizeFunctions();
            });
        }
        if (object.scrollFunctions != null && typeof(object.scrollFunctions) == "function") {
            $(window).on("scroll", function () {
                object.scrollFunctions();
            });
        }

        if (localStorage.getItem('group_id')>0) {
            $(".group-name").text(localStorage.getItem('group_name'));
            getSchedule(localStorage.getItem('group_id'));
        }

        $('select').on('change', function () {
            var select = $(this);
            if (select.val()) {
                var group_id = select.val();
                var select2Data = select.select2('data');
                var group_name = select2Data[0].text;
                localStorage.setItem('group_id', group_id);
                localStorage.setItem('group_name', group_name);
                $(".group-name").text(group_name);
                getSchedule(group_id);
            }
        });
        function getSchedule(group_id) {
            $.ajax({
                url: "/api/v1/schedules",
                data: "group=" + group_id,
                dataType: "json",
                success: function (data) {
                    mainTableBuilder();
                    allocateLessons(data);
                    object.afterTableLoad();
                    //localStorage.setItem('group_schedule', data);
                    $('select').val(null).trigger("change");
                }
            });
        }


        function allocateLessons(data) {
            for (var w = 0; w < data.length; w++) {
                var week = data[w];
                for (var d = 0; d < week['days'].length; d++) {
                    var day = week['days'][d];
                    for (var l = 0; l < day['events'].length; l++) {
                        var event = day['events'][l];
                        var lessonHtml = '' +
                        '<div class="less-wrap">' +
                            '<div class="less group-' + event['subgroup'] + '">' +
                                '<div class="title">' + event['course'] + '</div>' +
                                '<div class="ad">' + event['type'] + '' +
                                    '<div class="teacher">' + event['teacher'] + '</div>' +
                                '</div>' +
                                '<div class="aud">' + event['location'] + '</div>' +
                            '</div>' +
                            '<div class="info">' +
                                '<div class="edit-btn pencil-edit btn"></div>' +
                                '<div class="homework">http://www.dezinfo.net/images4/image/10.2013/education/education_03.jpg конспект по параграфу 5. П</div>' +
                                '<button class="submit-edit-btn hide">Готово</button>' +
                            '</div>'+
                        '</div>';
                        var lessonCell = $('#less-' + week['week_id'] + '-' + day['day_id'] + '-' + event['event_index']);
                        lessonCell.addClass('haveLess');
                        lessonCell.parent().removeClass('empty');
                        lessonCell.append(lessonHtml);
                    }
                }
            }
        }

        function mainTableBuilder() {

            $('.main-table-wrap').html(' ');
            var tableHtml = '';
            var days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
            for (w = 0; w < 2; w++) {
                if (w == 0) {
                    tableHtml += '<div class="week">' +
                                     '<div class="week-name">Нечётная</div>';
                } else {
                    tableHtml += '<div class="week hide">' +
                                     '<div class="week-name">Чётная</div>';
                }
                tableHtml +=         '<div class="subgroup-btns">' +
                                         '<div class="sb subgroup-btn-0">Все подгруппы</div>' +
                                         '<div class="sb subgroup-btn-1">Подгруппа 1</div>' +
                                         '<div class="sb subgroup-btn-2">Подгруппа 2</div>' +
                                         '<div class="sb subgroup-btn-3">Подгруппа 3</div>' +
                                     '</div>' +
                                     '<div class="main-table">' +
                                     '<table>' +
                                     '<tr>';
                for (d = 0; d < 6; d++) {
                    tableHtml +=        '<td class="col-xs-12 col-sm-6 col-md-4 day">' +
                                            '<div class="day-name">' + days[d] + '</div>' +
                                            '<table>';
                    for (l = 0; l < 8; l++) {
                        tableHtml +=           '<tr class="empty">' +
                                                    '<td class="less-' + (l + 1) + '" id="less-' + (w + 1) + '-' + (d + 1) + '-' + (l + 1) + '"></td>' +
                                               '</tr>';
                    }
                    tableHtml +=            '</table>' +
                                        '</td>';
                    if (d == 1 || d == 3) {
                        tableHtml +=    '<td class="col-sm-12 visible-sm separator"></td>';
                    }
                    if (d == 2) {
                        tableHtml +=    '<td class="col-md-12 visible-md visible-lg separator"></td>';
                    }
                }
                tableHtml +=         '</tr>' +
                                     '</table>' +
                                     '</div>' +
                                 '</div>';

            }
            $('.main-table-wrap').html(tableHtml);
        }
    }
};

