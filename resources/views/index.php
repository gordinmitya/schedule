<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,600&subset=latin,cyrillic-ext'
          rel='stylesheet' type='text/css'>
    <link href="css/template_styles.css" type="text/css" rel="stylesheet"/>
    <script src="js/scripts.js" type="text/javascript"></script>
</head>
<body>

<?php include 'imgRandom.php'; ?>
<header>
    <div class="top-menu clearfix">

    </div>
</header>
<section class="search-section">
    <div class="group-search" style="background-image: url(images/girls/<?php echo $filelist[$rand]; ?>)">
        <form action="" class="search-form">
            <select name="group-select" class="head-select"></select>
        </form>
    </div>
</section>


<main>
    <div class="page-wrap">
        <div class="content clearfix">
            <div class="head clearfix">
                <div class="date-wrap">
                    <div class="time">
                        <div class="hours"></div><span>:</span><div class="minutes"></div>
                    </div>
                    <div class="weekday"></div><div class="date"></div>
                </div>
                <div class="group-name"></div>
                <form action="" class="head-search-form">
                    <select name="" class="head-group-select"></select>
                </form>

            </div>
            <div class="main-table-wrap">

            </div>
        </div>
    </div>
</main>

<footer>
</footer>
</body>
</html>
