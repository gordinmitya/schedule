<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeekType extends Model
{
    public $fillable = ['id','name'];
    public $timestamps = false;
}
