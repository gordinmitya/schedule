<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DaysOfWeek extends Model
{
	public $fillable = ['id','name'];
    public $timestamps = false;
}
