<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/lessonsTableBuilder', function () {
    return view('lessonsTableBuilder');
});

Route::group(['prefix' => 'api/v1'], function() {
    	/*Route::resource('groups', 'GroupsApiController', ['only'=>['index', 'show']]);
    	Route::resource('courses', 'CoursesApiController', ['only'=>['index', 'show']]);*/
        Route::group(['prefix' => 'groups'], function() {
            Route::get('/','GroupsApiController@index');
            Route::get('/{group_id}/schedule', 'SchedulesApiController@index');
            
            Route::get('/{group_id}/updates/schedule', 'SchedulesApiController@updated_at');    
        });

        Route::group(['prefix' => 'teachers'], function() {
            Route::get('/','TeachersApiController@index');
            Route::get('/{teacher_id}/schedule', 'SchedulesApiController@TeacherIndex');   
        });
    	
    	/*Route::any( '{catchall}', function ( $page ) {
    		return response()->json(new stdClass(), 404);
		})->where('catchall', '(.*)');*/
    });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
	//
});
