<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Teacher;

class TeachersApiController extends Controller
{
    public function index(Request $request) {
    	$q=$request->input('q');
    	if (!empty($q)) {
    		$res = Teacher::select('id', 'name')->where('name', 'LIKE', $q.'%')->get();
    	} else {
    		$res = Teacher::select('id', 'name')->get();
    	}
    	return response()->json($res, 200, ['Content-Type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);

    }
    public function show($id) {
    	return response()->json(Teacher::findOrFail($id), 200, ['Content-Type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
    public function search($q) {
    	return response()->json($q);
    }
}