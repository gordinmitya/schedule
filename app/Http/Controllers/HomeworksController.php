<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeworksController extends Controller
{
    public function index($groupId, $timestamp = 0, $courseId = null)
    {
        if (empty($groupId)) {
            return response()->json(new stdClass(), 400, ['Content-Type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        
    }
    
    public function show($id)
    {
    }
}
