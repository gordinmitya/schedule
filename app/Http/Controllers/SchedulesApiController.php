<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\WeekType;
use App\DaysOfWeek;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SchedulesApiController extends Controller
{
    public function index(Request $request) {
    	$group_id=$request->input('group');
    	if (intval($group_id) > 0) {
            $weeks = array();
            foreach (WeekType::all() as $value) {
                $weeks[$value->id] = $value->name;
            }
            $days = array();
            foreach (DaysOfWeek::all() as $value) {
                $days[$value->id] = $value->name;
            }
    		$db_res = DB::select("SELECT `week_id`, (SELECT `name` FROM `week_types` WHERE `id` = i.`week_id`) week,
    			`day_id`, (SELECT `name` FROM `days_of_weeks` WHERE `id` = i.`day_id`) day,
    			`event_index`, 
    			`course_id`, (SELECT `name` FROM `courses` WHERE `id` = i.`course_id`) course,
    			`type_id`, (SELECT `name` FROM `event_types` WHERE `id` = i.`type_id`) type,
    			`subgroup`,
    			`teacher_id`, (SELECT `name` FROM `teachers` WHERE `id` = i.`teacher_id`) teacher,
    			`location` 
    			FROM event_templates i WHERE `group_id` = $group_id 
                ORDER BY `week_id`, `day_id`, `event_index`, `subgroup`");
            $raw_result = array(1=>array(), 2=>array());
            for ($i = 0; $i < count($db_res); $i++) {
                $info = [
                    "event_index" => $db_res[$i]->event_index,
                    "course_id" => $db_res[$i]->course_id,
                    "course" => $db_res[$i]->course,
                    "type_id" => $db_res[$i]->type_id,
                    "type" => $db_res[$i]->type,
                    "subgroup" => $db_res[$i]->subgroup,
                    "teacher_id" => $db_res[$i]->teacher_id,
                    "teacher" => $db_res[$i]->teacher,
                    "location" => $db_res[$i]->location,
                ];
                if ($db_res[$i]->week_id == 3) {
                    $raw_result[1][$db_res[$i]->day_id][] = $info;
                    $raw_result[2][$db_res[$i]->day_id][] = $info;
                } else {
                    $raw_result[$db_res[$i]->week_id][$db_res[$i]->day_id][] = $info;
                }
            }
            $result = array();
            foreach ($raw_result as $week_id => $value) {
                $days_of_week = array();
                foreach ($value as $day_id => $value) {
                    $days_of_week[] = [
                        "day_id" => $day_id,
                        "day" => $days[$day_id],
                        "events" => $value
                    ];
                }
                $result[] = [
                    "week_id" => $week_id,
                    "week" => $weeks[$week_id],
                    "days" => $days_of_week,
                ];
            }
    		return response()->json($result, 200, ['Content-Type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    	}
    	return response()->json([], 404, ['Content-Type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
}
