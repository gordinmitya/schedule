<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    public $fillable = ['id','name'];
    public $timestamps = false;
}
