<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTemplate extends Model
{
    public $fillable = ['id','week_id', 'day_id', 'event_index', 'course_id', 'type_id', 'subgroup', 'teacher_id', 'group_id', 'location'];
    public $timestamps = false;
}
