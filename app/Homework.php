<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Homework extends Model
{
    public $fillable = ['id','header', 'course_id', 'group_id', 'text', 'dead_line', 'author'];
}
