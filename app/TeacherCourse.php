<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherCourse extends Model
{
    public $fillable = ['teacher_id','course_id'];
    public $timestamps = false;
}
